# clone sourc code from bitbucket
* git clone https://pigdome@bitbucket.org/pigdome/marc2csv.git

# convert marc to csv

* $perl MARC2CSV.pl [marc] [csv] [map_header]


# generate csv to item
* $cd /srv/work/SAFBuilder
* $./safbuilder.sh -c  [csv] 

# move item to collections folder
* $cd ..
* $perl move_items_to_collection.pl [path Simpleachive...] [path to item]

# import item to dspace 
* /srv/punsarn/dspace-testing/bin/dspace import -a -e [email] -c [collections] -s [src] -m [mapfile]

