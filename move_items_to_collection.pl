#use File::Find::Rule;
use strict;
use warnings;

my $basedir = $ARGV[0];
my $desdir = $ARGV[1];
opendir(HOMEDIR,$basedir) || die ("Unable to open directory");

while (my $filename = readdir(HOMEDIR)) 
{ 
    my $subdir = $basedir. "/" . $filename;
    if( -d $subdir )
    {
        if( length($filename) > 3 ) 
        {
            open(my $fh,$subdir . "/collections") or next;
            while (my $row = <$fh>) 
            {
                $row =~ s/^\s+|\s+$//g;
                if( index($row,"/") >= 0 )
                {
                    #print $subdir . " : $row\n";
                    system("mkdir","$desdir/$row") if ! -e "$desdir/$row";
                    #system("cp","-R",$subdir,"/srv/work/collection/$row/" . $count++);
                    my $count = "";
                    while( $count eq "" )
                    {
                        $count = int(rand(1000000));
                        $count = "" if -e "$desdir/$row/$count";
                    }
                    system("mv",$subdir,"$desdir/$row/" . $count);    
                }
            }
        }
    } 
}
closedir(HOMEDIR); 